from __future__ import print_function
import io
import os
import json
import time
from googleapiclient.http import MediaIoBaseDownload
from auth import Auth
from googleapiclient.discovery import build

SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly',
              'https://www.googleapis.com/auth/drive.file',
              'https://www.googleapis.com/auth/drive.install',
              'https://www.googleapis.com/auth/drive.apps.readonly',
              'https://www.googleapis.com/auth/drive.metadata',
              'https://www.googleapis.com/auth/drive',
              'https://www.googleapis.com/auth/drive.activity',
              'https://www.googleapis.com/auth/drive.activity.readonly',
              'https://www.googleapis.com/auth/drive.readonly',
              'https://www.googleapis.com/auth/drive.metadata.readonly',
              'https://www.googleapis.com/auth/drive.scripts',
              ]

class GoogleDriveDownloader:
    def __init__(self, SCOPES):
        self.SCOPES = SCOPES

        # auth
        auth = Auth(self.SCOPES)
        creds = auth.getCredentials()

        self.drive_service = build('drive', 'v3', credentials=creds)

    def hash_count_files(self, count_files):
        hash_dir = os.getcwd()

        count_files = {"count_files": count_files}

        with open(hash_dir+'/count_files.json', 'w') as json_file:
            json.dump(count_files, json_file)

    def get_hashed_count_files(self):
        hash_dir = os.getcwd()

        try:
            stat = os.stat(hash_dir + '/count_files.json')
            date_modification = stat.st_mtime

            if time.time() - 85500 < date_modification:
                with open(hash_dir + '/count_files.json') as json_file:
                    data = json.load(json_file)

                    return data['count_files']
            else:
                return None
        except FileNotFoundError:
            return None

    def get_files(self):
        # Call the Drive v3 API
        if self.get_hashed_count_files() is None:
            count_files = 10000
            try:
                results = self.drive_service.files().list(
                    pageSize=count_files, fields="nextPageToken, files(id, name)").execute()
                print("Count files", count_files)
            except:
                # if count files in google drive less than 10000
                count_files = 9990
                condition = True
                while condition:
                    try:
                        results = self.drive_service.files().list(
                            pageSize=count_files, fields="nextPageToken, files(id, name)").execute()

                        break
                    except:
                        count_files -= 15

                        if count_files <= 40:
                            while True:
                                try:
                                    results = self.drive_service.files().list(
                                        pageSize=count_files, fields="nextPageToken, files(id, name)").execute()

                                    condition = False
                                    break
                                except:
                                    count_files -= 1
                        print("Count files:", count_files)

            # hash count files to file
            self.hash_count_files(count_files)
        else:
            count_files = self.get_hashed_count_files()

            results = self.drive_service.files().list(
                pageSize=count_files, fields="nextPageToken, files(id, name)").execute()


        items = results.get('files', [])

        # files = {"file_name": file_id}
        files = {}

        if not items:
            print('No files found.')
        else:
            # print('Files:')
            for item in items:
                files[item['name']] = item['id']
                # print(u'{0} ({1})'.format(item['name'], item['id']))
        return files

    def get_folders(self):
        # Call the Drive v3 API
        if self.get_hashed_count_files() is None:
            count_files = 10000
            try:
                results = self.drive_service.files().list(
                    pageSize=count_files, fields="nextPageToken, files(id, name, mimeType)").execute()
                print("Count files", count_files)
            except:
                # if count files in google drive less than 10000
                count_files = 9990
                condition = True
                while condition:
                    try:
                        results = self.drive_service.files().list(
                            pageSize=count_files, fields="nextPageToken, files(id, name, mimeType)").execute()

                        break
                    except:
                        count_files -= 15

                        if count_files <= 40:
                            while True:
                                try:
                                    results = self.drive_service.files().list(
                                        pageSize=count_files, fields="nextPageToken, files(id, name, mimeType)").execute()

                                    condition = False
                                    break
                                except:
                                    count_files -= 1
                        print("Count files:", count_files)

            # hash count files to file
            self.hash_count_files(count_files)
        else:
            count_files = self.get_hashed_count_files()

            results = self.drive_service.files().list(
                pageSize=count_files, fields="nextPageToken, files(id, name, mimeType)").execute()

        items = results.get('files', [])

        folders = {}

        if not items:
            return None
        else:
            for item in items:
                if str(item['mimeType']) == str('application/vnd.google-apps.folder'):
                    folders[item['name']] = item['id']
            return folders

    def get_content_of_folder(self, folder_id):
        page_token = None
        files = {}

        while True:
            response = self.drive_service.files().list(q="'{}' in parents".format(folder_id),
                                                  fields='nextPageToken, files(id, name)',
                                                  pageToken=page_token).execute()
            for file in response.get('files', []):
                # Process change
                files[str(file.get('name'))] = str(file.get('id'))
                print('Found file: %s (%s)' % (file.get('name'), file.get('id')))
            page_token = response.get('nextPageToken', None)
            if page_token is None:
                break

        return files

    def create_permission(self, folder_id, email_address):
        auth = Auth(self.SCOPES)
        creds = auth.getCredentials()

        drive_service = build('drive', 'v3', credentials=creds)

        def callback(request_id, response, exception):
            if exception:
                # Handle error
                print(exception)
            else:
                print("Permission Id: %s" % response.get('id'))

        batch = drive_service.new_batch_http_request(callback=callback)
        user_permission = {
            'type': 'user',
            'role': 'reader',
            'emailAddress': email_address
        }
        batch.add(drive_service.permissions().create(
            fileId=folder_id,
            body=user_permission,
            fields='id',
        ))
        batch.execute()

    def download_users_file(self, file_id, file_name):
        auth = Auth(self.SCOPES)
        creds = auth.getCredentials()

        drive_service = build('drive', 'v3', credentials=creds)
        request = drive_service.files().get_media(fileId=file_id)
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)

        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print("Download %d%%." % int(status.progress() * 100))

        with io.open(os.getcwd() + '/files/' + file_name, 'wb') as f:
            fh.seek(0)
            f.write(fh.read())

    def download_google_file(self, file_id, file_name):
        auth = Auth(self.SCOPES)
        creds = auth.getCredentials()

        drive_service = build('drive', 'v3', credentials=creds)
        request = drive_service.files().export_media(fileId=file_id,
                                                     mimeType='application/pdf')
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()

        with io.open(os.getcwd() + '/files/' + file_name + '.jpg', 'wb') as f:
            fh.seek(0)
            f.write(fh.read())
